# Jest Deep Mock

[![npm version](https://badge.fury.io/js/jest-deep-mock.svg)](https://badge.fury.io/js/jest-deep-mock) [![Build status](https://ci.appveyor.com/api/projects/status/ln4y9u49t70aow8p?svg=true)](https://ci.appveyor.com/project/benbeeson/jest-deep-mock)

Provides an alternative mock implementation to jest.fn() allowing to walk a mock object without specifying the value at each level. If no value is set for a property or no return value when calling a mock function, a new mock object will be returned.

The interface follows https://facebook.github.io/jest/docs/en/mock-function-api.html#content and the same expects can be done per https://facebook.github.io/jest/docs/en/expect.html#content.


## Getting Started

Install jest-deep-mock using npm:

```
npm install --save-dev jest-deep-mock
```

Write your first test using DeepMock:

```javascript
const DeepMock = require("jest-deep-mock")

test('set return value and assert response', () => {
    // GIVEN
    let mock = new DeepMock()
    mock.nested.func.mockReturnValue("value")

    // WHEN
    let actual = mock.nested.func(1, 2)

    // THEN
    expect(actual).toBe("value")

    expect(mock.nested.func).toHaveBeenCalledWith(1, 2)
})
```

Add the following section to your `package.json`:

```json
{
  "scripts": {
    "test": "jest"
  }
}
```

Run `npm test` to run the test with the following output:

```
 PASS  ./example.test.js
  ✓ set return value and assert response (11ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.005s
Ran all test suites.
```
