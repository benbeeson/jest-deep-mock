const BLACKLISTED_TARGET_ATTRIBUTES = ["name"]
const BLACKLISTED_DATA_ATTRIBUTES = ["then", "calls"]


function DeepMock(implementation) {
    let config = {
        returnConfig: {
            defaultSet: false,
            defaultValue: undefined,
            values: []
        },
        implementationConfig: {
            defaultSet: false,
            defaultValue: undefined,
            values: []
        },
        throwConfig: {
            defaultSet: false,
            defaultValue: undefined,
            values: []
        }
    }

    let proxy = null
    let data = {}

    let target = function() {}
    target.prototype = Object.create(DeepMock.prototype)
    target._isMockFunction = true
    target.mock = {
        calls: [],
        instances: []
    }

    target.mockReturnValue = (value) => {
        config.returnConfig.defaultSet = true
        config.returnConfig.defaultValue = value

        return target
    }
    target.mockReturnValueOnce = (value) => {
        config.returnConfig.values.push(value)

        return target
    }
    target.mockImplementation = (value) => {
        config.implementationConfig.defaultSet = true
        config.implementationConfig.defaultValue = value

        return target
    }
    target.mockImplementationOnce = (value) => {
        config.implementationConfig.values.push(value)

        return target
    }
    target.mockThrowValue = (value) => {
        config.throwConfig.defaultSet = true
        config.throwConfig.defaultValue = value

        return target
    }
    target.mockThrowValueOnce = (value) => {
        config.throwConfig.values.push(value)

        return target
    }
    target.mockReset = () => {
        target.mock = {
            calls: [],
            instances: []
        }
    }
    target.inspect = () => {
        return data
    }

    if(implementation)
    {
        target.mockImplementation(implementation)
    }

    function executeFunction(argumentsList) {
        if(config.implementationConfig.values.length)
        {
            return config.implementationConfig.values.shift().apply(proxy, argumentsList)
        }
        else if(config.implementationConfig.defaultSet)
        {
            return config.implementationConfig.defaultValue.apply(proxy, argumentsList)
        }

        if(config.throwConfig.values.length)
        {
            throw config.throwConfig.values.shift()
        }
        else if(config.throwConfig.defaultSet)
        {
            throw config.throwConfig.defaultValue
        }

        if(config.returnConfig.values.length)
        {
            return config.returnConfig.values.shift()
        }
        else if(config.returnConfig.defaultSet)
        {
            return config.returnConfig.defaultValue
        }

        return new DeepMock()
    }

    proxy = new Proxy(target, {
        get: (target, property, receiver) => {
            if(data[property] !== undefined)
            {
                return data[property]
            }

            if(!BLACKLISTED_TARGET_ATTRIBUTES.includes(property) && (
                typeof property !== "string" || target[property] !== undefined))
            {
                return target[property]
            }

            if(!BLACKLISTED_DATA_ATTRIBUTES.includes(property))
            {
                data[property] = new DeepMock()
            }

            return data[property]
        },
        set: (target, property, value, receiver) => {
            data[property] = value
        },
        apply: (target, thisArg, argumentsList) => {
            target.mock.calls.push(argumentsList)

            return executeFunction(argumentsList)
        },
        construct: (target, argumentsList, newTarget) => {
            target.mock.calls.push(argumentsList)

            let value = executeFunction(argumentsList)
            target.mock.instances.push(value)
            
            Object.setPrototypeOf(value, target.prototype)

            return value
        },
        defineProperty: function(target, property, descriptor) {
            Object.defineProperty(data, property, descriptor)

            return true
        },
        ownKeys: function(target) {
            return Object.keys(data).concat([
                "arguments", "caller", "prototype"
            ])
        }
    })

    Object.setPrototypeOf(proxy, target.prototype)

    return proxy
}

module.exports = DeepMock