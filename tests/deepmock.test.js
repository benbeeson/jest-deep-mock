let DeepMock


beforeEach(() => {
    DeepMock = require("../src/deepmock.js")
})


describe("Properties", () => {
    let mock

    beforeEach(() => {
        mock = new DeepMock()
    })

    test("get property", () => {
        // WHEN
        let actual = mock.property

        // THEN
        expect(actual).toBeInstanceOf(DeepMock)
    })

    test("get nested property", () => {
        // WHEN
        let actual = mock.nested.property

        // THEN
        expect(actual).toBeInstanceOf(DeepMock)
    })

    test("get blacklisted property", () => {
        // WHEN
        let actual = mock.then

        // THEN
        expect(actual).toBe(undefined)
    })

    test("get name property", () => {
        // WHEN
        let actual = mock.name

        // THEN
        expect(actual).toBeInstanceOf(DeepMock)
    })

    test("set property", () => {
        // WHEN
        mock.property = "value"

        // THEN
        expect(mock.property).toBe("value")
    })

    test("set nested property", () => {
        // WHEN
        mock.nested.property = "value"

        // THEN
        expect(mock.nested.property).toBe("value")
    })

    test("set name property", () => {
        // GIVEN
        mock.name = "value"

        // WHEN
        let actual = mock.name

        // THEN
        expect(actual).toBe("value")
    })

    test("get keys", () => {
        // GIVEN
        mock.name = "value"
        mock.test = "item"

        // WHEN
        let actual = Object.getOwnPropertyNames(mock)

        // THEN
        expect(actual).toEqual([
            "name", "test", "arguments", "caller", "prototype"
        ])
    })
})

describe("Functions", () => {
    let mock

    beforeEach(() => {
        mock = new DeepMock()
    })

    test("call mock no arguments", () => {
        // WHEN
        mock()

        // THEN
        expect(mock.mock.calls).toEqual([[]])
    })

    test("call mock with arguments", () => {
        // WHEN
        mock(1, 2)

        // THEN
        expect(mock.mock.calls).toEqual([[1, 2]])
    })

    test("call mock multiple times", () => {
        // WHEN
        mock(1, 2)
        mock(3, 4)

        // THEN
        expect(mock.mock.calls).toEqual([
            [1, 2], [3, 4]
        ])
    })

    test("return default", () => {
        // GIVEN
        mock.mockReturnValue(1)

        // WHEN
        let one = mock()
        let two = mock()

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(1)
    })

    test("return once no default", () => {
        // GIVEN
        mock.mockReturnValueOnce(1)
        mock.mockReturnValueOnce(2)

        // WHEN
        let one = mock()
        let two = mock()
        let three = mock()

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(2)
        expect(three).toBeInstanceOf(DeepMock)
    })

    test("return once with default return", () => {
        // GIVEN
        mock.mockReturnValueOnce(1)
        mock.mockReturnValue(2)

        // WHEN
        let one = mock()
        let two = mock()
        let three = mock()

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(2)
        expect(three).toBe(2)
    })

    test("throw default", () => {
        // GIVEN
        mock.mockThrowValue(new Error("One"))

        // WHEN
        expect(mock).toThrowError("One")
        expect(mock).toThrowError("One")
    })

    test("throw once no default", () => {
        // GIVEN
        mock.mockThrowValueOnce(new Error("One"))
        mock.mockThrowValueOnce(new Error("Two"))

        // WHEN
        expect(mock).toThrowError("One")
        expect(mock).toThrowError("Two")

        let three = mock()

        // THEN
        expect(three).toBeInstanceOf(DeepMock)
    })

    test("throw once with default throw", () => {
        // GIVEN
        mock.mockThrowValueOnce(new Error("One"))
        mock.mockThrowValue(new Error("Two"))

        // WHEN
        expect(mock).toThrowError("One")
        expect(mock).toThrowError("Two")
        expect(mock).toThrowError("Two")
    })

    test("implementation default", () => {
        // GIVEN
        mock.mockImplementation(() => {
            return 1
        })

        // WHEN
        let one = mock("one")
        let two = mock("two")

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(1)

        expect(mock.mock.calls).toEqual([["one"], ["two"]])
    })

    test("implementation once no default", () => {
        // GIVEN
        mock.mockImplementationOnce(() => {
            return 1
        })
        mock.mockImplementationOnce(() => {
            return 2
        })

        // WHEN
        let one = mock("one")
        let two = mock("two")
        let three = mock("three")

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(2)
        expect(three).toBeInstanceOf(DeepMock)

        expect(mock.mock.calls).toEqual([["one"], ["two"], ["three"]])
    })

    test("implementation once with default", () => {
        // GIVEN
        mock.mockImplementationOnce(() => {
            return 1
        })
        mock.mockImplementationOnce(() => {
            return 2
        })
        mock.mockImplementation(() => {
            return 3
        })

        // WHEN
        let one = mock("one")
        let two = mock("two")
        let three = mock("three")

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(2)
        expect(three).toBe(3)

        expect(mock.mock.calls).toEqual([["one"], ["two"], ["three"]])
    })

    test("implementation passed via constructor", () => {
        // GIVEN
        mock = new DeepMock(() => {
            return 1
        })

        // WHEN
        let one = mock("one")
        let two = mock("two")

        // THEN
        expect(one).toBe(1)
        expect(two).toBe(1)

        expect(mock.mock.calls).toEqual([["one"], ["two"]])
    })

    test("implementation default with arguments", () => {
        // GIVEN
        mock.mockImplementation((a, b) => {
            return a + b
        })

        // WHEN
        let actual = mock(1, 2)

        // THEN
        expect(actual).toBe(3)

        expect(mock.mock.calls).toEqual([[1, 2]])
    })

    test("implementation one with arguments", () => {
        // GIVEN
        mock.mockImplementationOnce((a, b) => {
            return a + b
        })

        // WHEN
        let actual = mock(1, 2)

        // THEN
        expect(actual).toBe(3)

        expect(mock.mock.calls).toEqual([[1, 2]])
    })

    test("implementation this", () => {
        // GIVEN
        mock.a = 1
        mock.b = 2
        mock.mockImplementation(function() {
            return this.a + this.b
        })

        // WHEN
        let actual = mock()

        // THEN
        expect(actual).toBe(3)

        expect(mock.mock.calls).toEqual([[]])
    })
})

describe("Constructors", () => {
    let Mock

    beforeEach(() => {
        Mock = new DeepMock()
    })

    test("construct mock no arguments", () => {
        // WHEN
        let instance = new Mock()

        // THEN
        expect(Mock.mock.calls).toEqual([[]])
        expect(Mock.mock.instances).toEqual([instance])
    })

    test("construct mock with arguments", () => {
        // WHEN
        let instance = new Mock(1, 2)

        // THEN
        expect(Mock.mock.calls).toEqual([[1, 2]])
        expect(Mock.mock.instances).toEqual([instance])
    })

    test("construct mock multiple times", () => {
        // WHEN
        let one = new Mock(1, 2)
        let two = new Mock(3, 4)

        // THEN
        expect(Mock.mock.calls).toEqual([
            [1, 2], [3, 4]
        ])
        expect(Mock.mock.instances).toEqual([one, two])
    })

    test("return default", () => {
        // GIVEN
        let instance = {}

        Mock.mockReturnValue(instance)

        // WHEN
        let one = new Mock()
        let two = new Mock()

        // THEN
        expect(Mock.mock.instances).toEqual([instance, instance])
    })
})

describe("State", () => {
    let mock

    beforeEach(() => {
        mock = new DeepMock()
    })

    test("reset empty state", () => {
        // GIVEN
        expect(mock.mock).toEqual({
            calls: [],
            instances: []
        })

        // WHEN
        mock.mockReset()

        // THEN
        expect(mock.mock).toEqual({
            calls: [],
            instances: []
        })
    })

    test("reset state", () => {
        // GIVEN
        let instance = new mock(1)

        expect(mock.mock).toEqual({
            calls: [[1]],
            instances: [instance]
        })
        
        // WHEN
        mock.mockReset()

        // THEN
        expect(mock.mock).toEqual({
            calls: [],
            instances: []
        })
    })
})

describe("Debugging", () => {
    let mock

    beforeEach(() => {
        mock = new DeepMock()
    })

    test("inspect string value", () => {
        // GIVEN
        mock.test = "value"

        // WHEN
        let actual = mock.inspect()

        // THEN
        expect(actual).toEqual({test: "value"})
    })
})

describe("Instance checks", () => {
    test("instanceof DeepMock", () => {
        // GIVEN
        const mock = new DeepMock()

        //WHEN
        const actual = mock instanceof DeepMock

        // THEN
        expect(actual).toBe(true)
    })

    describe("Mock Classes", () => {
        let MockClassOne, MockClassTwo, instance

        beforeEach(() => {
            // GIVEN
            MockClassOne = new DeepMock()
            MockClassTwo = new DeepMock()

            instance = new MockClassOne()
        })

        test("instanceof DeepMock", () => {
            // WHEN
            const actual = instance instanceof DeepMock

            // THEN
            expect(actual).toBe(true)
        })

        test("instanceof MockClassOne", () => {
            // WHEN
            const actual = instance instanceof MockClassOne

            // THEN
            expect(actual).toBe(true)
        })

        test("not instanceof MockClassTwo", () => {
            // WHEN
            const actual = instance instanceof MockClassTwo

            // THEN
            expect(actual).toBe(false)
        })
    })
})

describe("Defined properties", () => {
    let mock

    beforeEach(() => {
        mock = new DeepMock()
    })

    test("getter", () => {
        // GIVEN
        Object.defineProperty(mock, "test", {
            get: () => {
                return "value"
            }
        })

        // WHEN
        const actual = mock.test

        // THEN
        expect(actual).toBe("value")
    })

    test("setter", () => {
        // GIVEN
        let setter = new DeepMock()

        Object.defineProperty(mock, "test", {
            set: (value) => {
                setter(value)
            }
        })

        // WHEN
        mock.test = "value"

        // THEN
        expect(setter).toHaveBeenCalledWith("value")
    })

    test("value", () => {
        // GIVEN
        Object.defineProperty(mock, "test", {
            value: () => {
                return "value"
            }
        })

        // WHEN
        const actual = mock.test()

        // THEN
        expect(actual).toBe("value")
    })
})
